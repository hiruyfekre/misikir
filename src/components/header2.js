import { Link } from "gatsby";
import React, { useState } from "react"
import { StaticImage } from "gatsby-plugin-image"



function Header() {
  const [isExpanded, toggleExpansion] = useState(false);
  const [navbarOpen, setNavbarOpen] = useState(false)
  const handleToggle = () => {
    setNavbarOpen(prev => !prev)
  }
  

  

  return ( 

    <header className="fixed grid grid-cols-6 top-0  h-[70px] z-20  w-full   " >
             
         <button
          className=" bg-white-500 hover:-translate-y-1 hover:scale-110  duration-300 items-end block z-[100] px-3 fixed top-0 right-0 py-2 text-black     rounded md:hidden"
          onClick={() => toggleExpansion(!isExpanded)} 
        >
          <p onClick={handleToggle}>{navbarOpen ? <span >Close</span> : <span className="text-white">Menu</span> } </p>
        </button>
      
        <nav
          className={`${
            isExpanded ? `grid` : `hidden`
          }   " w-full fixed md:block bg-white top-0 items-center right-0  text-right   h-full md:h-[70px] z-[50] md:text-center  md:bg-transparent md-h-auto md:right-[50%] md:translate-x-2/4	 md:-mt-6  container" lg:w-auto  `}
        >
          {[
            {
              route: `/about`,
              title: `About Us`,
            },
            {
              route: `/completedprojects`,
              title: `Selected Works`,
            },
            {
              route: `/contact`,
              title: `Contact`,
            },
            
          ].map((link) => (
            <Link 
              className="  text-center  text-black md:text-white  top-0 mt-0 text-[18px] px-4 md:inline-block md:mt-8 md:ml-6"
              key={link.title}
              to={link.route}
            >
              {link.title}
            </Link>
          ))}
        </nav>
    
    </header>
  );
}

export default Header;
