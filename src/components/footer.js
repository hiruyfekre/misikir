import React, { useEffect , useState} from "react";
import { StaticImage } from "gatsby-plugin-image"
import { Link } from "gatsby";


function Footer () {
  
   const [count, setCount] = useState(0);  
    useEffect(() => {
        
        document.title =  `You clicked ${count} times`;
        
});

   return (
    <footer className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-4 column-gap-12 	grid-cols-4 gap-x-30px items-end ">
        <h1 className=" w-auto    pb-[6px]  "><a href="/"><StaticImage  className="relative w-[30%] h-[30%] "src="/home/summitcambridge1/Documents/my-new-website/src/images/Mhfullblack.svg" alt="Misikir Architects"/></a></h1>
      
      <p className="">Megenagna Building, 1st floor<br/>Addis Ababa, Ethiopia</p>
      
      <ul className="col col-span-1"><li>091105050505 </li><li><a href="mailto:info@misikirarchitects.com">info@misikirarchitects.com</a></li></ul>
      <p className="align-end block text-right m-0"> <Link to="/">Back to top </Link> </p>
    </footer>


   )

}

export default Footer; 