import PropTypes from "prop-types";
import React from "react";
import Footer from "./footer";

import Header from "./header";

function Layout({ children }) {
  return (
     <> 

     <Header /> 
        {children}
      <Footer /> 
     </> 
  );
}

Layout.propTypes = {
  children: PropTypes.node.isRequired,
};

export default Layout;
