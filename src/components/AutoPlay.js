import React, { useEffect } from "react";
import 'react-awesome-slider/dist/custom-animations/cube-animation.css';
import { Link } from "gatsby";
import { StaticImage } from "gatsby-plugin-image"
import AwesomeSlider from 'react-awesome-slider';
import withAutoplay from 'react-awesome-slider/dist/autoplay';
import red from "../images/red.jpg"
import sitefoto from "../images/misikirsite.png"
import blackish from "../images/blackish.jpg"
import brick from "../images/brick.jpg";
import builder from "../images/builders.jpg";
import loud from "../images/loud.png"
import jilan from "../images/tailwind-icon.png";
import tekeste from "../images/tekeste.jpg"
import workers from "../images/workers.png"
import shrubs from "../images/shrubs.jpg"
import miskiratwork from "../images/Misikir_Architect_photo.png"
import building from "../images/building.jpg"
import one from "../images/1.png"
import two from "../images/2.png"
import nest from "../images/birdnest.png"


import AwesomeSliderStyles from "react-awesome-slider/dist/styles.css";
import AnimationStyles from 'react-awesome-slider/src/styled/fold-out-animation/fold-out-animation.scss';
import styles from './style.css'







const AutoPlay = () => {
    const AutoPlaySlider = withAutoplay(AwesomeSlider);


    return (
        <div >

            <AutoPlaySlider

                buttons={true}
                play={true}
                infinite
                fillParent
                cancelOnInteraction={false}
                interval={24000}
            animation="foldOutAnimation"
            bullets={false}
>



            <div data-src={nest}  >

                
                <blockquote className="z-10 absolute text-center text-white  text-[1rem]  md:text-[1.5rem] top-[65%] md:top-[75%] bottom-[50%] right-0 left-0 w-full"> <q> In the last 10 years we have guided dozens of our happy clients to success in a variety of building projects here in Addis.
                    
                </q>
                    <br />
                    <cite className="text-lg  p-3 text-white"> Lead Architect</cite>

                </blockquote>


            </div>






            

            <div className="pointer-events-none" data-src={sitefoto}>
                <h1 className="relative   w-full h-full  ">
                
                </h1>


                <blockquote className="z-10 absolute text-center p-x-4 text-white  text-[1rem]  md:text-[1.5rem] top-[65%] md:top-[80%]  bottom-[50%] right-0 left-0 w-full  "> <q> Very professional and great to work with!
                </q>
                    <br />
                    <cite className="text-lg">Tekeste Gebremicheal - owner of luxury apartment building around the Bole Printing Press </cite>

                </blockquote>
            </div>

            <div data-src={miskiratwork} >
                <h1 className="relative w-full h-full  ">
                    
                </h1>

                <blockquote className="z-10 absolute text-center text-[1.5rem] md:text-[1.5rem] px-3 text-white  top-[65%] md:top:[10%] bottom-[45%] right-0 left-0 w-full  "> <q>They exceeded our expectations
                </q>
                    <br />
                    <cite className="text-lg">Ibrahim  - Project Developer of Jialan Real Estate in Old Airport Area</cite>

                </blockquote>
            </div>


            <div className="pointer-events-none" data-src={builder} >
                <h1 className="relative   w-full h-full  ">
                    
                </h1>

                <blockquote className="z-10 absolute text-center text-white text-[2rem] md:text-[1.5rem] top-[65%] md:top-[80%] bottom-[50%] right-0 left-0 w-full  "> <q>We saved money and got a home that is always complimented by our guests
                </q>
                    <br />
                    <cite className="text-lg">Tamrat and Helen - G-3 Residence in Haile Garment</cite>

                </blockquote>
            </div>






        </AutoPlaySlider >
        </div >
    )




}


export default AutoPlay 