import { Link } from "gatsby";
import React, { useState } from "react"
import { StaticImage } from "gatsby-plugin-image"





function Header() {
  const [isExpanded, toggleExpansion] = useState(false);
  const [navbarOpen, setNavbarOpen] = useState(false)
  const handleToggle = () => {
    setNavbarOpen(prev => !prev)
  }
  
  const activeStyle = { color:'grey' }

  

  return ( 

    <header className="fixed grid grid-cols-6 top-0   bg-white h-[70px] z-20  w-full   " >
            
  <Link to="/">

                 <StaticImage className=" w-[30%] md:w-[40%]  h-auto mt-3 md:mt-3 ml-8 md:ml-12 " src="../images/mh.svg" alt="Misikir_Logo" />      
            </Link>
         <button
          className="  items-end block z-[100] px-3 fixed top-0 right-0 py-2 text-black     rounded md:hidden"
          onClick={() => toggleExpansion(!isExpanded)} 
        >
          <p className=" " onClick={handleToggle}>{navbarOpen ? "Close" : " Menu"} </p>
        </button>
        <nav
          className={`${
            isExpanded ? `translate-x-0` : `translate-x-full`
          } " grid ease-in-out duration-500
          w-full fixed  md:block  bg-white top-0 right-0 items-center     h-full  md:h-[70px] z-[50] md-text-center md-h-auto md:right-[50%] md:translate-x-2/4 md:-mt-6 container  lg:w-auto " `}
        >
          {[
            {
              route: `/about`,
              title: `About Us`,
            },
            {
              route: `/completedprojects`,
              title: `Selected Works`,
            },
            {
              route: `/contact`,
              title: `Contact`,
            },
            
          ].map((link) => (
            <Link
              className=" focus-visible:outline  transition duration-150  text-center  top-0  mt-0 text-[18px] px-4 md:inline-block md:mt-8 md:ml-6"
              key={link.title}
              to={link.route}
              activeStyle={activeStyle}

            >
              {link.title}
            </Link>
          ))}
        </nav>
      
    
    </header>
  );
}

export default Header;
