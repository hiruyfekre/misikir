import React, { useState, useEffect } from "react";

import Typist from 'react-typist';

function Tiles() {

	const [count, setCount] = useState(1);

	useEffect(() => {
		//document.title `you clicked count times`
		console.log("Count: " + count);
		setCount(1);
	}, [count]);

	return (
		<>

			{count ? (
				<Typist
					className="TypistExample-header"
					avgTypingDelay={40}
					onTypingDone={() => setCount(0)}>
			
						<span>Design</span> <Typist.Delay ms={1250} />
						<Typist.Backspace count={20} delay={800} />

						<span>Planning</span>  <Typist.Delay ms={1250} />
						<Typist.Backspace count={20} delay={800} />

						<span>and</span>  <Typist.Delay ms={1250} />
						<Typist.Backspace count={20} delay={800} />

						<span>Construction</span> <Typist.Delay ms={1250} />
						<Typist.Backspace count={20} delay={800} />

						
					
				</Typist>
			) : (
				""
			)}

		</>)
}


export default Tiles