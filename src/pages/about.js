import React, { useState, useCallback } from "react";
import Layout from "../components/layout";
//import SEO from "../components/seo";
import Gallery from 'react-photo-gallery';
import Carousel, { Modal, ModalGateway } from "react-images";
import { photos } from "../photos";
import Header from "../components/header"
import { StaticImage } from "gatsby-plugin-image"
import { Link } from "gatsby";
import styles from "./style.css"
import PageTransition from 'gatsby-plugin-page-transitions';




function AboutPage() {
  return (
    <Layout>
      <div name="back-to-top" className="flex flex-col bg-[#f5f5f5]  max-w-[calc(100% - 35px - 35px)]">
        <section className=" grid md:grid-cols-2    md:row-span-2 images w-[750px] mt-[112px] mx-auto gap-[30px] mb-0   mt-[112px] mx-auto  mb-0">

          <PageTransition
            defaultStyle={{
              transition: 'top 500ms cubic-bezier(0.47, 0, 0.75, 0.72)',
              top: '100%',
              position: 'absolute',
              width: '100%',
              left: '68%'
            }}
            transitionStyles={{
              entering: { top: '50%' },
              entered: { top: '50%' },
              exiting: { top: '100%' }
            }}
            transitionTime={500}
          >


            <div>
              
            </div>
          </PageTransition>


          <figure>
            <StaticImage width={980}
              height={1440} src="../images/misikirsite.png" alt="Misikir Architecture site image" />

          </figure>

          <div className=" text-xl">
            <p className=" text-xl ">
              Over the past 10 years we have been very active in the Ethiopian construction industry designing and managing the construction of dozens of construction projects. Our architects and graduate engineers are licensed by the Ministry of Urban Development and Construction. </p>
            <p className="mt-32 pt-24"> Our company&apos;s lead architect recently received and MFA at the Alle School of Fine Arts and Design and he has a strong passion for photography
            </p>
            
          </div>

        </section>
        <section className="grid md:grid-cols-2 gap-x-8 gap-y-[70px]  images w-[960px]
    max-w-[calc(100% - 35px - 35px)]
  text-left 
  mt-[168px] mx-auto">
          <figure className="">
            {" "}
            <StaticImage
              width={980}
              height={1387}
              src="../images/misikir_shadow_worker.png"
              alt=""
            />
            <figcaption className="text-xl mt-4">‘12 story building project’ 2014.</figcaption>
          </figure>
          <figure className="">
            {" "}
            <StaticImage
              width={980}
              height={1387}
              src="../images/misikir_stairway.png"
              alt=""
            />
            <figcaption className="text-xl mt-4">Lead architect inspecting stairway at luxury apartment project 2016</figcaption>
          </figure>
          <figure className="">
            {" "}
            <StaticImage
              width={980}
              height={1000}
              src="../images/misikir_office.png"
              alt=""
            />
            <figcaption className="text-xl mt-4"> Settting up shop. First few weeks at Misikir Architects current head office near Megenagna, 2019</figcaption>
          </figure>
          <figure className="">
            {" "}
            <StaticImage
              width={980}
              height={1000}
              src="../images/loud.png"
              alt=""
            /> <figcaption className="text-xl mt-4">Lead architect is out of town using photography, capturing a solemn event  , 2019 </figcaption>

          </figure>
          <figure>
            <StaticImage
              width={980}
              height={1387}
              src="../images/misikir_site_panaroma.png"
              alt=""
            />    <figcaption className="text-xl mt-4">
              Interesting light and shadow view from a &apos; eight-story  project in March, 2018
            </figcaption>
          </figure>
          <figure>
            <StaticImage
              width={980}
              height={1387}
              src="../images/Misikir_Architect_photo.png"
              alt=""
            />          <figcaption className="text-xl mt-4">
              Taken during contract supervision of a 17 story apartment complex , 2017
            </figcaption>
          </figure>
          <div className="bg-green-700"> </div>
        </section>
      </div>

    </Layout>
  );
}

export default AboutPage;
