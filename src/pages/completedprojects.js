import React, { useState, useCallback } from "react";
import Layout from "../components/layout";
//import SEO from "../components/seo";
import Gallery from 'react-photo-gallery';
import Carousel, { Modal, ModalGateway } from "react-images";
import { photos } from "../photos";
import Header from "../components/header";


function CompletedProjects() {
  const [currentImage, setCurrentImage] = useState(0);
  const [viewerIsOpen, setViewerIsOpen] = useState(false);

  const openLightbox = useCallback((event, { photo, index }) => {
    setCurrentImage(index);
    setViewerIsOpen(true);
  }, []);

  const closeLightbox = () => {
    setCurrentImage(0);
    setViewerIsOpen(false);
  
    
  };
  const customStyles = {
    view: () => ({
        // none of react-images styles are passed to <View />
        position: 'relative',
        '& > img': {
            position: relative ,
            margin:'0 auto',
            padding: 0,
            width: '',
            height: ''
        },
    })
};
  
    return (
       <>
       <Layout> 
        <Header /> 
       <div className="mb-12 px-20 ">
      
        <Gallery photos={photos} styles={customStyles} onClick={openLightbox} />
        <ModalGateway>
        {viewerIsOpen ? (
          <Modal onClose={closeLightbox}>
            <Carousel
              currentIndex={currentImage}
            

              views={photos.map(x => ({
                ...x,
                srcset: x.srcSet,
                caption: x.title
              }))}
            />
          </Modal>
        ) : null}
      </ModalGateway>
      </div>
      
      </Layout>
    </> 
    )
  }


export default CompletedProjects


