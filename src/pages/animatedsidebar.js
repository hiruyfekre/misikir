/*  marc 16  https://blog.avneesh.tech/create-an-animated-sidebar-with-tailwindcss-in-react   */
import React from 'react';

import Sidebar from "../components/sidebar";

function App() {
  return (
    <div className="flex flex-col items-center justify-center min-h-screen py-2">
      <Sidebar />
      <span className="flex h-8 w-8">
  <span className="animate-ping absolute inline-flex h-full w-full rounded-full bg-sky-400 opacity-75"></span>
  <span className="relative inline-flex rounded-full h-3 w-3 bg-sky-500"></span>
</span>
    </div>
  );
}

export default App;