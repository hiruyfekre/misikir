import React , {useEffect, useState} from "react";

import Header from "../components/header";
import SEO from "../components/seo";
import { StaticImage } from "gatsby-plugin-image"
import HeaderIndex from "../components/header2";
import  AutoPlay  from "../components/AutoPlay";

const IndexPage = (props) => {
  const [count, setCount] = useState(0);

  const {location} = props;
  const url = location.pathname ? location.pathname : '';

  useEffect(() => {
    // Update the document title using the browser API
    document.title = `You clicked ${count} times`;    
 

  },[]);
  return (
         
       <>
         <HeaderIndex /> 
         <button type="button" className="bg-indigo-500 ..." disabled>
  <svg className="animate-spin h-5 w-5 mr-3 ..." viewBox="0 0 24 24">
  </svg>
  Processing...
</button>
          
           <AutoPlay />
           
     
         </>   
          );
}






export default IndexPage;


